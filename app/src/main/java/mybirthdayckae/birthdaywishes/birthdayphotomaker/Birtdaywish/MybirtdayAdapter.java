package mybirthdayckae.birthdaywishes.birthdayphotomaker.Birtdaywish;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.uncopt.android.widget.text.justify.JustifiedTextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.R;

import java.util.ArrayList;

public class MybirtdayAdapter extends RecyclerView.Adapter<MybirtdayAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<String> places;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        JustifiedTextView myMsg;
        Button shareButton;

        public MyViewHolder(@NonNull View view) {
            super(view);
            this.myMsg = (JustifiedTextView) view.findViewById(R.id.loveText);
            this.shareButton = (Button) view.findViewById(R.id.btnShare);
        }
    }

    public MybirtdayAdapter(Context context2, ArrayList<String> arrayList) {
        this.context = context2;
        this.places = arrayList;
    }

    @NonNull
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new MyViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.love_list, viewGroup, false));
    }

    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        myViewHolder.myMsg.setText((CharSequence) this.places.get(i));
        final String str = (String) this.places.get(i);
        myViewHolder.shareButton.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent("android.intent.action.SEND");
                intent.setType("text/plain");
                intent.putExtra("android.intent.extra.TEXT", str);
                view.getContext().startActivity(Intent.createChooser(intent, "Share Text"));
            }
        });
    }

    public int getItemCount() {
        return this.places.size();
    }
}