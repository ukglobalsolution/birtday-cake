package mybirthdayckae.birthdaywishes.birthdayphotomaker.Birtdaywish;

import android.content.Context;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.R;

import java.util.ArrayList;
import java.util.Arrays;

public class BirthdayWishActivity extends AppCompatActivity {
    Context context;
    RecyclerView lovequotes;
    ArrayList<String> places = new ArrayList<>(Arrays.asList(new String[]{
            "I hope your special day will bring you lots of happiness, love, and fun. You deserve them a lot. Enjoy!",
            "All things are sweet and bright. May you have a lovely birthday Night"
    ,"Don’t ever change! Stay as amazing as you are, my friend"
            ,"Let’s light the candles and celebrate this special day of your life. Happy birthday."
            ,"Here’s to the sweetest and loveliest person I know. Happy birthday!"
            ,"Happy birthday to my best friend, the one I care about the most!"
            ,"Wherever your feet may take, whatever endeavor you lay hands on. It will always be successful. Happy birthday."
            ,"May this special day bring you endless joy and tons of precious memories!"
            ,"You are very special and that’s why you need to float with lots of smiles on your lovely face. Happy birthday."
            ,"Let your all the dreams to be on fire and light your birthday candles with that. Have a gorgeous birthday."
    }));
    private String[] title = new String[0];

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_love_quotes);
        this.context = this;
        this.lovequotes = (RecyclerView) findViewById(R.id.quoteslist);
        this.lovequotes.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        this.lovequotes.setAdapter(new MybirtdayAdapter(this.context, this.places));
    }
}