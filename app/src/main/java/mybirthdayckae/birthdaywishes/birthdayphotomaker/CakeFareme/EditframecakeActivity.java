package mybirthdayckae.birthdaywishes.birthdayphotomaker.CakeFareme;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.Adapter.MyFrameAdapter;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.Adapter.MyStickerAdapter;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.Album.ShareActivity;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.crop.CropImageActivity;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.DemoStickerTExt.DemoStickerView;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.DemoStickerTExt.StickerImageView;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.Effectsdemo.Effects;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.Glob;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.Interface.ItemClickFrame;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.Interface.ItemClickSticker;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.MyTouch.MultiTouchListener;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.R;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.Text.TextActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

@SuppressLint("WrongConstant")
public class EditframecakeActivity extends AppCompatActivity implements OnClickListener, ItemClickFrame, ItemClickSticker {
    private static final String TAG = "hello";
    public static String _uri2;
    public static Bitmap mainframebitmap;
    private int REQUEST_TEXT = 101;
    ArrayList<String> assetlist;
    Bitmap bitmapsticker;
    ImageView btnBrightness;
    ImageView btnEffect;
    ImageView btnFlip;
    ImageView btnFrame;
    ImageView btnSave;
    ImageView btnSticker;
    ImageView btnText;
    Context context;
    private ImageView ef1;
    private ImageView ef10;
    private ImageView ef11;
    private ImageView ef12;
    private ImageView ef14;
    private ImageView ef15;
    private ImageView ef16;
    private ImageView ef17;
    private ImageView ef18;
    private ImageView ef19;
    private ImageView ef2;
    private ImageView ef20;
    private ImageView ef21;
    private ImageView ef22;
    private ImageView ef4;
    private ImageView ef5;
    private ImageView ef6;
    private ImageView ef7;
    private ImageView ef9;
    private ImageView ef_original;
    ImageView frameImage;
    RecyclerView framerecyclerView;
    HorizontalScrollView horizontalscroll;
    private FrameLayout mainframforsave;
    ImageView myImage;
    private DemoStickerView.OnTouchSticker onTouchSticker = new DemoStickerView.OnTouchSticker() {
        public void onTouchedSticker() {
            EditframecakeActivity.this.removeBorder();
        }
    };
    SeekBar sb_value;
    /* access modifiers changed from: private */
    public StickerImageView stickerImageView;
    ArrayList<Integer> stickerid = new ArrayList<>();
    RecyclerView stickerrecyclerView;
    private int view_id;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.activity_editimage);
        this.context = this;
        bind();
    }

    private void bind() {
        this.mainframforsave = (FrameLayout) findViewById(R.id.mainframforsave);
        this.mainframforsave.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                EditframecakeActivity.this.removeBorder();
                return false;
            }
        });
        this.ef_original = (ImageView) findViewById(R.id.ef_original);
        this.ef1 = (ImageView) findViewById(R.id.ef1);
        this.ef2 = (ImageView) findViewById(R.id.ef2);
        this.ef4 = (ImageView) findViewById(R.id.ef4);
        this.ef5 = (ImageView) findViewById(R.id.ef5);
        this.ef6 = (ImageView) findViewById(R.id.ef6);
        this.ef7 = (ImageView) findViewById(R.id.ef7);
        this.ef9 = (ImageView) findViewById(R.id.ef9);
        this.ef14 = (ImageView) findViewById(R.id.ef14);
        this.ef15 = (ImageView) findViewById(R.id.ef15);
        this.ef16 = (ImageView) findViewById(R.id.ef16);
        this.ef17 = (ImageView) findViewById(R.id.ef17);
        this.ef18 = (ImageView) findViewById(R.id.ef18);
        this.ef19 = (ImageView) findViewById(R.id.ef19);
        this.ef20 = (ImageView) findViewById(R.id.ef20);
        this.ef21 = (ImageView) findViewById(R.id.ef21);
        this.ef22 = (ImageView) findViewById(R.id.ef22);
        this.ef_original.setOnClickListener(this);
        this.ef1.setOnClickListener(this);
        this.ef2.setOnClickListener(this);
        this.ef4.setOnClickListener(this);
        this.ef5.setOnClickListener(this);
        this.ef6.setOnClickListener(this);
        this.ef7.setOnClickListener(this);
        this.ef9.setOnClickListener(this);
        this.ef14.setOnClickListener(this);
        this.ef15.setOnClickListener(this);
        this.ef16.setOnClickListener(this);
        this.ef17.setOnClickListener(this);
        this.ef18.setOnClickListener(this);
        this.ef19.setOnClickListener(this);
        this.ef20.setOnClickListener(this);
        this.ef21.setOnClickListener(this);
        this.ef22.setOnClickListener(this);
        Effects.applyEffectNone(this.ef_original);
        Effects.applyEffect1(this.ef1);
        Effects.applyEffect2(this.ef2);
        Effects.applyEffect4(this.ef4);
        Effects.applyEffect5(this.ef5);
        Effects.applyEffect6(this.ef6);
        Effects.applyEffect7(this.ef7);
        Effects.applyEffect9(this.ef9);
        Effects.applyEffect14(this.ef14);
        Effects.applyEffect15(this.ef15);
        Effects.applyEffect16(this.ef16);
        Effects.applyEffect17(this.ef17);
        Effects.applyEffect18(this.ef18);
        Effects.applyEffect19(this.ef19);
        Effects.applyEffect20(this.ef20);
        Effects.applyEffect21(this.ef21);
        Effects.applyEffect22(this.ef22);
        this.myImage = (ImageView) findViewById(R.id.myImage);
        this.myImage.setImageBitmap(CropImageActivity.cropbitmap);
        this.myImage.setOnTouchListener(new MultiTouchListener());
        this.framerecyclerView = (RecyclerView) findViewById(R.id.framerecyclerView);
        this.stickerrecyclerView = (RecyclerView) findViewById(R.id.stickerrecyclerView);
        this.btnEffect = (ImageView) findViewById(R.id.btnEffect);
        this.btnFrame = (ImageView) findViewById(R.id.btnFrame);
        this.btnSticker = (ImageView) findViewById(R.id.btnSticker);
        this.btnSave = (ImageView) findViewById(R.id.btnSave);
        this.btnText = (ImageView) findViewById(R.id.btnText);
        this.btnBrightness = (ImageView) findViewById(R.id.btnBrightness);
        this.btnFlip = (ImageView) findViewById(R.id.btnFlip);
        this.frameImage = (ImageView) findViewById(R.id.frameImage);
        this.sb_value = (SeekBar) findViewById(R.id.sb_value);
        this.horizontalscroll = (HorizontalScrollView) findViewById(R.id.horizontalscroll);
        this.btnFrame.setOnClickListener(this);
        this.btnSticker.setOnClickListener(this);
        this.btnText.setOnClickListener(this);
        this.btnBrightness.setOnClickListener(this);
        this.btnFrame.setOnClickListener(this);
        this.btnEffect.setOnClickListener(this);
        this.btnSave.setOnClickListener(this);
        this.btnFlip.setOnClickListener(this);
        this.sb_value.setProgress(100);
        this.sb_value.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
                EditframecakeActivity.this.myImage.setAlpha(((float) i) / 250.0f);
            }
        });
        Animation loadAnimation = AnimationUtils.loadAnimation(this, R.anim.left);
        Animation loadAnimation2 = AnimationUtils.loadAnimation(this, R.anim.right);
        this.btnFrame.startAnimation(loadAnimation);
        this.btnBrightness.startAnimation(loadAnimation2);
        this.btnSticker.startAnimation(loadAnimation);
        this.btnText.startAnimation(loadAnimation2);
        this.btnEffect.startAnimation(loadAnimation);
        this.btnFlip.startAnimation(loadAnimation2);
        this.btnSave.startAnimation(loadAnimation);
    }


    public void onClick(View view) {
        int id = view.getId();
        if (id != R.id.btnBrightness) {
            switch (id) {
                case R.id.btnEffect /*2131296315*/:
                    if (this.horizontalscroll.getVisibility() == 8) {
                        this.horizontalscroll.setVisibility(0);
                        this.sb_value.setVisibility(8);
                        this.stickerrecyclerView.setVisibility(8);
                        this.framerecyclerView.setVisibility(8);
                        return;
                    }
                    this.horizontalscroll.setVisibility(8);
                    return;
                case R.id.btnFlip /*2131296316*/:
                    ImageView imageView = this.myImage;
                    float f = -180.0f;
                    if (imageView.getRotationY() == -180.0f) {
                        f = 0.0f;
                    }
                    imageView.setRotationY(f);
                    return;
                case R.id.btnFrame /*2131296317*/:
                    if (this.framerecyclerView.getVisibility() == 8) {
                        this.framerecyclerView.setVisibility(0);
                        this.sb_value.setVisibility(8);
                        this.stickerrecyclerView.setVisibility(8);
                        this.horizontalscroll.setVisibility(8);
                    } else {
                        this.framerecyclerView.setVisibility(8);
                    }
                    this.assetlist = new ArrayList<>();
                    this.assetlist.clear();
                    asset("frame");
                    this.framerecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), 0, false));
                    this.framerecyclerView.setAdapter(new MyFrameAdapter(this.context, this.assetlist, this));
                    return;
                default:
                    switch (id) {
                        case R.id.btnSave /*2131296320*/:
                            removeBorder();
                            mainframebitmap = getbitmap(this.mainframforsave);
                            saveImage(mainframebitmap);
                            startActivity(new Intent(this.context, ShareActivity.class));
                            return;
                        case R.id.btnSticker /*2131296321*/:
                            if (this.stickerrecyclerView.getVisibility() == 8) {
                                this.stickerrecyclerView.setVisibility(0);
                                this.framerecyclerView.setVisibility(8);
                                this.sb_value.setVisibility(8);
                                this.horizontalscroll.setVisibility(8);
                            } else {
                                this.stickerrecyclerView.setVisibility(8);
                            }
                            this.assetlist = new ArrayList<>();
                            this.assetlist.clear();
                            asset("sticker");
                            this.stickerrecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), 0, false));
                            this.stickerrecyclerView.setAdapter(new MyStickerAdapter(this.context, this.assetlist, this));
                            return;
                        case R.id.btnText /*2131296322*/:
                            startActivityForResult(new Intent(this, TextActivity.class), this.REQUEST_TEXT);
                            return;
                        default:
                            switch (id) {
                                case R.id.ef1 /*2131296369*/:
                                    Effects.applyEffect1(this.myImage);
                                    return;
                                case R.id.ef14 /*2131296370*/:
                                    Effects.applyEffect14(this.myImage);
                                    return;
                                case R.id.ef15 /*2131296371*/:
                                    Effects.applyEffect15(this.myImage);
                                    return;
                                case R.id.ef16 /*2131296372*/:
                                    Effects.applyEffect16(this.myImage);
                                    return;
                                case R.id.ef17 /*2131296373*/:
                                    Effects.applyEffect17(this.myImage);
                                    return;
                                case R.id.ef18 /*2131296374*/:
                                    Effects.applyEffect18(this.myImage);
                                    return;
                                case R.id.ef19 /*2131296375*/:
                                    Effects.applyEffect19(this.myImage);
                                    return;
                                case R.id.ef2 /*2131296376*/:
                                    Effects.applyEffect2(this.myImage);
                                    return;
                                case R.id.ef20 /*2131296377*/:
                                    Effects.applyEffect20(this.myImage);
                                    return;
                                case R.id.ef21 /*2131296378*/:
                                    Effects.applyEffect21(this.myImage);
                                    return;
                                case R.id.ef22 /*2131296379*/:
                                    Effects.applyEffect22(this.myImage);
                                    return;
                                case R.id.ef4 /*2131296380*/:
                                    Effects.applyEffect4(this.myImage);
                                    return;
                                case R.id.ef5 /*2131296381*/:
                                    Effects.applyEffect5(this.myImage);
                                    return;
                                case R.id.ef6 /*2131296382*/:
                                    Effects.applyEffect6(this.myImage);
                                    return;
                                case R.id.ef7 /*2131296383*/:
                                    Effects.applyEffect7(this.myImage);
                                    return;
                                case R.id.ef9 /*2131296384*/:
                                    Effects.applyEffect9(this.myImage);
                                    return;
                                case R.id.ef_original /*2131296385*/:
                                    Effects.applyEffectNone(this.myImage);
                                    return;
                                default:
                                    return;
                            }
                    }
            }
        } else if (this.sb_value.getVisibility() == 8) {
            this.sb_value.setVisibility(0);
            this.framerecyclerView.setVisibility(8);
            this.stickerrecyclerView.setVisibility(8);
            this.horizontalscroll.setVisibility(8);
        } else {
            this.sb_value.setVisibility(8);
        }
    }

    private void saveImage(Bitmap bitmap) {
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/"+ Glob.Edit_Folder_name);
        if (!myDir.exists()) {
            myDir.mkdirs();
        }
        String format = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

        String fname = "Image-"+ format +".jpg";
        File file = new File (myDir, fname);


        StringBuilder rajesh_uri=new StringBuilder();
        rajesh_uri.append(Environment.getExternalStorageDirectory());
        rajesh_uri.append("/");
        rajesh_uri.append(Glob.Edit_Folder_name);
        rajesh_uri.append("/");
        rajesh_uri.append(fname);

        String myImageUri=rajesh_uri.toString();
        Glob._url=myImageUri;

        if (file.exists ())
            file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
            getApplicationContext().sendBroadcast(new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE", Uri.fromFile(file)));


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private Bitmap getbitmap(View view) {
        Bitmap createBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Config.ARGB_8888);
        view.draw(new Canvas(createBitmap));
        return createBitmap;
    }

    private void asset(String str) {
        String[] strArr = new String[0];
        try {
            String[] list = getResources().getAssets().list(str);
            if (list != null) {
                for (String append : list) {
                    ArrayList<String> arrayList = this.assetlist;
                    StringBuilder sb = new StringBuilder();
                    sb.append(str);
                    sb.append("/");
                    sb.append(append);
                    arrayList.add(sb.toString());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setOnClickFrame(int i) {
        try {
            this.frameImage.setImageBitmap(BitmapFactory.decodeStream(getAssets().open(String.valueOf(this.assetlist.get(i)))));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setOnClickSticker(int i) {
        try {
            this.bitmapsticker = BitmapFactory.decodeStream(getAssets().open(String.valueOf(this.assetlist.get(i))));
            addsticker();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, @Nullable Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == this.REQUEST_TEXT && i2 == -1) {
            addtext();
        }
    }

    private void addsticker() {
        this.stickerImageView = new StickerImageView(this, this.onTouchSticker);
        this.stickerImageView.setImageBitmap(this.bitmapsticker);
        this.view_id = new Random().nextInt();
        int i = this.view_id;
        if (i < 0) {
            this.view_id = i - (i * 2);
        }
        this.stickerImageView.setId(this.view_id);
        this.stickerid.add(Integer.valueOf(this.view_id));
        this.stickerImageView.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                EditframecakeActivity.this.stickerImageView.setControlItemsHidden(false);
            }
        });
        this.mainframforsave.addView(this.stickerImageView);
    }

    private void addtext() {
        this.stickerImageView = new StickerImageView(this, this.onTouchSticker);
        this.stickerImageView.setImageBitmap(TextActivity.finalBitmapText);
        this.view_id = new Random().nextInt();
        int i = this.view_id;
        if (i < 0) {
            this.view_id = i - (i * 2);
        }
        this.stickerImageView.setId(this.view_id);
        this.stickerid.add(Integer.valueOf(this.view_id));
        this.stickerImageView.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                EditframecakeActivity.this.stickerImageView.setControlItemsHidden(false);
            }
        });
        this.mainframforsave.addView(this.stickerImageView);
    }

    /* access modifiers changed from: private */
    public void removeBorder() {
        for (int i = 0; i < this.stickerid.size(); i++) {
            View findViewById = this.mainframforsave.findViewById(((Integer) this.stickerid.get(i)).intValue());
            if (findViewById instanceof StickerImageView) {
                ((StickerImageView) findViewById).setControlItemsHidden(true);
            }
        }
    }


}
