package mybirthdayckae.birthdaywishes.birthdayphotomaker.Nameoncake;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import java.io.IOException;
import java.util.ArrayList;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.R;

public class CustomGrid extends BaseAdapter {
    private ArrayList<String> assetlist;
    private Context context;

    public class ViewHolder {
        ImageView frameItem;

        public ViewHolder() {
        }
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public CustomGrid(Context context2, ArrayList<String> arrayList) {
        this.context = context2;
        this.assetlist = arrayList;
    }

    public int getCount() {
        return this.assetlist.size();
    }

    public Object getItem(int i) {
        return this.assetlist.get(i);
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder = new ViewHolder();
        if (view == null) {
            view = LayoutInflater.from(this.context).inflate(R.layout.grid_single, null);
        }
        viewHolder.frameItem = (ImageView) view.findViewById(R.id.grid_image);
        try {
            viewHolder.frameItem.setImageBitmap(BitmapFactory.decodeStream(this.context.getAssets().open(String.valueOf(this.assetlist.get(i)))));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return view;
    }
}