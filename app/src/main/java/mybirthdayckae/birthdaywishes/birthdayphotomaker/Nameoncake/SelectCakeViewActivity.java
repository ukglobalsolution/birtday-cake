package mybirthdayckae.birthdaywishes.birthdayphotomaker.Nameoncake;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import androidx.appcompat.app.AppCompatActivity;
import java.io.IOException;
import java.util.ArrayList;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.R;

public class SelectCakeViewActivity extends AppCompatActivity {
    public static Bitmap cakebitmap;
    private CustomGrid adapter;
    ArrayList<String> assetlist;
    Context context;
    GridView grid;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_certi_grid_view);
        this.context = this;
        bind();
    }

    private void bind() {
        this.grid = (GridView) findViewById(R.id.grid);
        this.assetlist = new ArrayList<>();
        this.assetlist.clear();
        asset("cacke");
        this.adapter = new CustomGrid(this.context, this.assetlist);
        this.grid.setAdapter(this.adapter);
        this.grid.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                try {
                    SelectCakeViewActivity.cakebitmap = BitmapFactory.decodeStream(SelectCakeViewActivity.this.getAssets().open(String.valueOf(SelectCakeViewActivity.this.assetlist.get(i))));
                   SelectCakeViewActivity.this.startActivity(new Intent(SelectCakeViewActivity.this.context, EditCackeActivity.class));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void asset(String str) {
        String[] strArr = new String[0];
        try {
            String[] list = getResources().getAssets().list(str);
            if (list != null) {
                for (String append : list) {
                    ArrayList<String> arrayList = this.assetlist;
                    StringBuilder sb = new StringBuilder();
                    sb.append(str);
                    sb.append("/");
                    sb.append(append);
                    arrayList.add(sb.toString());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}