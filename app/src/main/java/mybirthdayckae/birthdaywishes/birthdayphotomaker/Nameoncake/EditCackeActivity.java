package mybirthdayckae.birthdaywishes.birthdayphotomaker.Nameoncake;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.Adapter.MyFrameAdapter;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.Adapter.MyStickerAdapter;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.Album.ShareActivity;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.DemoStickerTExt.DemoStickerView;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.DemoStickerTExt.StickerImageView;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.Glob;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.Interface.ItemClickFrame;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.Interface.ItemClickSticker;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.R;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.Text.TextActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

@SuppressLint("WrongConstant")
public class EditCackeActivity extends AppCompatActivity implements View.OnClickListener, ItemClickFrame, ItemClickSticker {
    private static final String TAG = "hello";
    public static String _uri2;
    public static Bitmap mainframebitmap;
    private int REQUEST_TEXT = 101;
    ArrayList<String> assetlist;
    Bitmap bitmapsticker;
    ImageView btnFlip;
    ImageView btnFrame;
    ImageView btnSave;
    ImageView btnSticker;
    ImageView btnText;
    Context context;

    ImageView frameImage;
    RecyclerView framerecyclerView;
    HorizontalScrollView horizontalscroll;
    private FrameLayout mainframforsave;

    private DemoStickerView.OnTouchSticker onTouchSticker = new DemoStickerView.OnTouchSticker() {
        public void onTouchedSticker() {
            EditCackeActivity.this.removeBorder();
        }
    };
    /* access modifiers changed from: private */
    public StickerImageView stickerImageView;
    ArrayList<Integer> stickerid = new ArrayList<>();
    RecyclerView stickerrecyclerView;
    private int view_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_cacke);
        this.context = this;
        bind();
    }

    private void bind() {
        this.mainframforsave = (FrameLayout) findViewById(R.id.mainframforsave);
        this.mainframforsave.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                EditCackeActivity.this.removeBorder();
                return false;
            }
        });


        this.framerecyclerView = (RecyclerView) findViewById(R.id.framerecyclerView);
        this.stickerrecyclerView = (RecyclerView) findViewById(R.id.stickerrecyclerView);
        this.btnFrame = (ImageView) findViewById(R.id.btnFrame);
        this.btnSticker = (ImageView) findViewById(R.id.btnSticker);
        this.btnSave = (ImageView) findViewById(R.id.btnSave);
        this.btnText = (ImageView) findViewById(R.id.btnText);
        this.btnFlip = (ImageView) findViewById(R.id.btnFlip);
        this.frameImage = (ImageView) findViewById(R.id.frameImage);
        frameImage.setImageBitmap(SelectCakeViewActivity.cakebitmap);
        this.horizontalscroll = (HorizontalScrollView) findViewById(R.id.horizontalscroll);
        this.btnFrame.setOnClickListener(this);
        this.btnSticker.setOnClickListener(this);
        this.btnText.setOnClickListener(this);
        this.btnFrame.setOnClickListener(this);
        this.btnSave.setOnClickListener(this);
        this.btnFlip.setOnClickListener(this);

        Animation loadAnimation = AnimationUtils.loadAnimation(this, R.anim.bounce);
        Animation loadAnimation2 = AnimationUtils.loadAnimation(this, R.anim.right);
        this.btnFrame.startAnimation(loadAnimation);
        this.btnSticker.startAnimation(loadAnimation);
        this.btnText.startAnimation(loadAnimation);
        this.btnFlip.startAnimation(loadAnimation);
        this.btnSave.startAnimation(loadAnimation);
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id != R.id.btnBrightness) {
            switch (id) {
                case R.id.btnEffect:
                    view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.buttonclick));
                    if (this.horizontalscroll.getVisibility() == 8) {
                        this.horizontalscroll.setVisibility(0);
                        this.stickerrecyclerView.setVisibility(8);
                        this.framerecyclerView.setVisibility(8);
                        return;
                    }
                    this.horizontalscroll.setVisibility(8);
                    return;
                case R.id.btnFlip:
                    view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.buttonclick));
                    FrameLayout imageView = this.mainframforsave;
                    float f = -180.0f;
                    if (imageView.getRotationY() == -180.0f) {
                        f = 0.0f;
                    }
                    imageView.setRotationY(f);
                    return;
                case R.id.btnFrame:
                    view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.buttonclick));
                    if (this.framerecyclerView.getVisibility() == 8) {
                        this.framerecyclerView.setVisibility(0);
                        this.stickerrecyclerView.setVisibility(8);
                        this.horizontalscroll.setVisibility(8);
                    } else {
                        this.framerecyclerView.setVisibility(8);
                    }
                    this.assetlist = new ArrayList<>();
                    this.assetlist.clear();
                    asset("cacke");
                    this.framerecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), 0, false));
                    this.framerecyclerView.setAdapter(new MyFrameAdapter(this.context, this.assetlist, this));
                    return;
                case R.id.btnSave :
                    view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.buttonclick));
                    removeBorder();
                    mainframebitmap = getbitmap(this.mainframforsave);
                    saveImage(mainframebitmap);
                    startActivity(new Intent(this.context, ShareActivity.class));
                    return;
                case R.id.btnSticker :
                    view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.buttonclick));
                    if (this.stickerrecyclerView.getVisibility() == 8) {
                        this.stickerrecyclerView.setVisibility(0);
                        this.framerecyclerView.setVisibility(8);
                        this.horizontalscroll.setVisibility(8);
                    } else {
                        this.stickerrecyclerView.setVisibility(8);
                    }
                    this.assetlist = new ArrayList<>();
                    this.assetlist.clear();
                    asset("sticker");
                    this.stickerrecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), 0, false));
                    this.stickerrecyclerView.setAdapter(new MyStickerAdapter(this.context, this.assetlist, this));
                    return;
                case R.id.btnText :
                    view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.buttonclick));
                    startActivityForResult(new Intent(this, TextActivity.class), this.REQUEST_TEXT);
                    return;
                default:
                    return;
            }
        }
    }




    private void saveImage(Bitmap bitmap) {
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/" + Glob.Edit_Folder_name);
        if (!myDir.exists()) {
            myDir.mkdirs();
        }
        String format = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

        String fname = "Image-" + format + ".jpg";
        File file = new File(myDir, fname);


        StringBuilder rajesh_uri = new StringBuilder();
        rajesh_uri.append(Environment.getExternalStorageDirectory());
        rajesh_uri.append("/");
        rajesh_uri.append(Glob.Edit_Folder_name);
        rajesh_uri.append("/");
        rajesh_uri.append(fname);

        String myImageUri = rajesh_uri.toString();
        Glob._url = myImageUri;

        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
            getApplicationContext().sendBroadcast(new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE", Uri.fromFile(file)));


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private Bitmap getbitmap(View view) {
        Bitmap createBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        view.draw(new Canvas(createBitmap));
        return createBitmap;
    }

    private void asset(String str) {
        String[] strArr = new String[0];
        try {
            String[] list = getResources().getAssets().list(str);
            if (list != null) {
                for (String append : list) {
                    ArrayList<String> arrayList = this.assetlist;
                    StringBuilder sb = new StringBuilder();
                    sb.append(str);
                    sb.append("/");
                    sb.append(append);
                    arrayList.add(sb.toString());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setOnClickFrame(int i) {
        try {
            this.frameImage.setImageBitmap(BitmapFactory.decodeStream(getAssets().open(String.valueOf(this.assetlist.get(i)))));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setOnClickSticker(int i) {
        try {
            this.bitmapsticker = BitmapFactory.decodeStream(getAssets().open(String.valueOf(this.assetlist.get(i))));
            addsticker();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, @Nullable Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == this.REQUEST_TEXT && i2 == -1) {
            addtext();
        }
    }

    private void addsticker() {
        this.stickerImageView = new StickerImageView(this, this.onTouchSticker);
        this.stickerImageView.setImageBitmap(this.bitmapsticker);
        this.view_id = new Random().nextInt();
        int i = this.view_id;
        if (i < 0) {
            this.view_id = i - (i * 2);
        }
        this.stickerImageView.setId(this.view_id);
        this.stickerid.add(Integer.valueOf(this.view_id));
        this.stickerImageView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                EditCackeActivity.this.stickerImageView.setControlItemsHidden(false);
            }
        });
        this.mainframforsave.addView(this.stickerImageView);
    }

    private void addtext() {
        this.stickerImageView = new StickerImageView(this, this.onTouchSticker);
        this.stickerImageView.setImageBitmap(TextActivity.finalBitmapText);
        this.view_id = new Random().nextInt();
        int i = this.view_id;
        if (i < 0) {
            this.view_id = i - (i * 2);
        }
        this.stickerImageView.setId(this.view_id);
        this.stickerid.add(Integer.valueOf(this.view_id));
        this.stickerImageView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                EditCackeActivity.this.stickerImageView.setControlItemsHidden(false);
            }
        });
        this.mainframforsave.addView(this.stickerImageView);
    }

    /* access modifiers changed from: private */
    public void removeBorder() {
        for (int i = 0; i < this.stickerid.size(); i++) {
            View findViewById = this.mainframforsave.findViewById(((Integer) this.stickerid.get(i)).intValue());
            if (findViewById instanceof StickerImageView) {
                ((StickerImageView) findViewById).setControlItemsHidden(true);
            }
        }
    }

}
