package mybirthdayckae.birthdaywishes.birthdayphotomaker.DemoStickerTExt;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint.FontMetrics;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Build.VERSION;
import android.util.AttributeSet;
import android.widget.TextView;

@SuppressLint("AppCompatCustomView")
public class TextArtLibTextArtView extends TextView {
    private int getSeekbarvalue;
    private int getshadowcolor;
    private int id = 0;
    private Path path = new Path();
    private int seekbarvalue;

    public TextArtLibTextArtView(Context context) {
        super(context);
    }

    public int getmShadowColor() {
        return this.getshadowcolor;
    }

    public void setmShadowColor(int i) {
        this.getshadowcolor = i;
    }

    public TextArtLibTextArtView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public TextArtLibTextArtView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    @TargetApi(21)
    public TextArtLibTextArtView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.id != 0) {
            canvas.save();
            a(canvas, 0.0f, 0.0f, 1.0f);
            canvas.restore();
            return;
        }
        super.onDraw(canvas);
    }

    private void a(Canvas canvas, float f, float f2, float f3) {
        float f4;
        int i;
        float f5 = 0;
        String replace = getText().toString().replace("\n", " ");
        int i2 = this.id;
        int i3 = i2 + 6;
        if (i2 >= 360) {
            i3 = 359;
        } else if (i2 <= -360) {
            i3 = -359;
        }
        float measureText = getPaint().measureText(replace);
        if (getPaint().getStrokeWidth() > 0.0f) {
            measureText += getPaint().getStrokeWidth() * 2.0f;
            f4 = getPaint().getStrokeWidth() + 0.0f;
        } else {
            f4 = 0.0f;
        }
        double abs = (double) ((measureText * 360.0f) / ((float) Math.abs(i3)));
        Double.isNaN(abs);
        float f6 = (float) (abs / 3.141592653589793d);
        float width = (((((float) getWidth()) - 2.0f) - f6) / 2.0f) + ((float) ((int) f));
        float f7 = (float) ((int) f2);
        FontMetrics fontMetrics = getPaint().getFontMetrics();
        if (i3 > 0) {
            f5 = f7 - fontMetrics.ascent;
            if (getPaint().getStrokeWidth() > 0.0f) {
                f5 += getPaint().getStrokeWidth();
            }
            i = 270;
        } else {
            float height = f7 + ((((((float) getHeight()) * f3) - 2.0f) - f6) - fontMetrics.descent);
            if (getPaint().getStrokeWidth() > 0.0f) {
                height -= getPaint().getStrokeWidth();
            }
            i = 90;
        }
        this.path.reset();
        this.path.addArc(new RectF(width, f5, width + f6, f6 + f5), (float) (i - (i3 / 2)), (float) i3);
        canvas.drawTextOnPath(replace, this.path, f4, 0.0f, getPaint());
    }

    public int getCurvingAngle() {
        return this.id;
    }

    public void setCurvingAngle(int i) {
        this.id = i;
    }

    public int getTextSizeSeekBarValue() {
        return this.seekbarvalue;
    }

    public void setTextSizeSeekBarValue(int i) {
        this.seekbarvalue = i;
    }

    public void setShadowLayer(float f, float f2, float f3, int i) {
        getPaint().setShadowLayer(f, f2, f3, i);
        if (VERSION.SDK_INT >= 11) {
            setLayerType(1, getPaint());
        }
        invalidate();
        super.setShadowLayer(f, f2, f3, i);
    }

    public int getCurveseekbarvalue() {
        return this.getSeekbarvalue;
    }

    public void setCurveseekbarvalue(int i) {
        this.getSeekbarvalue = i;
    }
}
