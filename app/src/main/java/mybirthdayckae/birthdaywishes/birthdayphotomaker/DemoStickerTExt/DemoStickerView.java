package mybirthdayckae.birthdaywishes.birthdayphotomaker.DemoStickerTExt;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.R;

public abstract class DemoStickerView extends FrameLayout implements OnClickListener {
    private static final int BUTTON_SIZE_DP = 30;
    private static final int SELF_SIZE_DP = 100;
    public static final String TAG = "com.stickerView";
    /* access modifiers changed from: private */
    public double centerX;
    /* access modifiers changed from: private */
    public double centerY;
    private BorderView iv_border;
    private ImageView iv_delete;
    private ImageView iv_flip;
    private ImageView iv_scale;
    private OnTouchListener mTouchListener = new OnTouchListener() {
        public boolean onTouch(View view, MotionEvent motionEvent) {
            boolean equals = view.getTag().equals("DraggableViewGroup");
            int i = 0;
            String str = DemoStickerView.TAG;
            if (equals) {
                int action = motionEvent.getAction();
                if (action == 0) {
                    DemoStickerView.this.setControlItemsHidden(true);
                    DemoStickerView.this.onTouchSticker.onTouchedSticker();
                    Log.v(str, "sticker view action down");
                    DemoStickerView.this.move_orgX = motionEvent.getRawX();
                    DemoStickerView.this.move_orgY = motionEvent.getRawY();
                } else if (action == 1) {
                    Log.v(str, "sticker view action up");
                    DemoStickerView.this.setControlItemsHidden(false);
                } else if (action == 2) {
                    Log.v(str, "sticker view action move_right");
                    float rawX = motionEvent.getRawX() - DemoStickerView.this.move_orgX;
                    float rawY = motionEvent.getRawY() - DemoStickerView.this.move_orgY;
                    DemoStickerView demoStickerView = DemoStickerView.this;
                    demoStickerView.setX(demoStickerView.getX() + rawX);
                    DemoStickerView demoStickerView2 = DemoStickerView.this;
                    demoStickerView2.setY(demoStickerView2.getY() + rawY);
                    DemoStickerView.this.move_orgX = motionEvent.getRawX();
                    DemoStickerView.this.move_orgY = motionEvent.getRawY();
                }
            } else if (view.getTag().equals("iv_scale")) {
                int action2 = motionEvent.getAction();
                if (action2 == 0) {
                    Log.v(str, "iv_scale action down");
                    DemoStickerView demoStickerView3 = DemoStickerView.this;
                    demoStickerView3.this_orgX = demoStickerView3.getX();
                    DemoStickerView demoStickerView4 = DemoStickerView.this;
                    demoStickerView4.this_orgY = demoStickerView4.getY();
                    DemoStickerView.this.scale_orgX = motionEvent.getRawX();
                    DemoStickerView.this.scale_orgY = motionEvent.getRawY();
                    DemoStickerView demoStickerView5 = DemoStickerView.this;
                    demoStickerView5.scale_orgWidth = (double) demoStickerView5.getLayoutParams().width;
                    DemoStickerView demoStickerView6 = DemoStickerView.this;
                    demoStickerView6.scale_orgHeight = (double) demoStickerView6.getLayoutParams().height;
                    DemoStickerView.this.rotate_orgX = motionEvent.getRawX();
                    DemoStickerView.this.rotate_orgY = motionEvent.getRawY();
                    DemoStickerView demoStickerView7 = DemoStickerView.this;
                    demoStickerView7.centerX = (double) (demoStickerView7.getX() + ((View) DemoStickerView.this.getParent()).getX() + (((float) DemoStickerView.this.getWidth()) / 2.0f));
                    int identifier = DemoStickerView.this.getResources().getIdentifier("status_bar_height", "dimen", "android");
                    if (identifier > 0) {
                        i = DemoStickerView.this.getResources().getDimensionPixelSize(identifier);
                    }
                    double d = (double) i;
                    DemoStickerView demoStickerView8 = DemoStickerView.this;
                    double y = (double) (demoStickerView8.getY() + ((View) DemoStickerView.this.getParent()).getY());
                    Double.isNaN(y);
                    Double.isNaN(d);
                    double d2 = y + d;
                    double height = (double) (((float) DemoStickerView.this.getHeight()) / 2.0f);
                    Double.isNaN(height);
                    demoStickerView8.centerY = d2 + height;
                } else if (action2 == 1) {
                    Log.v(str, "iv_scale action up");
                } else if (action2 == 2) {
                    Log.v(str, "iv_scale action move_right");
                    DemoStickerView.this.rotate_newX = motionEvent.getRawX();
                    DemoStickerView.this.rotate_newY = motionEvent.getRawY();
                    double atan2 = Math.atan2((double) (motionEvent.getRawY() - DemoStickerView.this.scale_orgY), (double) (motionEvent.getRawX() - DemoStickerView.this.scale_orgX));
                    double access$500 = (double) DemoStickerView.this.scale_orgY;
                    double access$1100 = DemoStickerView.this.centerY;
                    Double.isNaN(access$500);
                    double d3 = access$500 - access$1100;
                    double access$400 = (double) DemoStickerView.this.scale_orgX;
                    double access$1000 = DemoStickerView.this.centerX;
                    Double.isNaN(access$400);
                    double abs = (Math.abs(atan2 - Math.atan2(d3, access$400 - access$1000)) * 180.0d) / 3.141592653589793d;
                    StringBuilder sb = new StringBuilder();
                    sb.append("angle_diff: ");
                    sb.append(abs);
                    Log.v(str, sb.toString());
                    DemoStickerView demoStickerView9 = DemoStickerView.this;
                    double access$1400 = demoStickerView9.getLength(demoStickerView9.centerX, DemoStickerView.this.centerY, (double) DemoStickerView.this.scale_orgX, (double) DemoStickerView.this.scale_orgY);
                    DemoStickerView demoStickerView10 = DemoStickerView.this;
                    String str2 = str;
                    double access$14002 = demoStickerView10.getLength(demoStickerView10.centerX, DemoStickerView.this.centerY, (double) motionEvent.getRawX(), (double) motionEvent.getRawY());
                    int access$1500 = DemoStickerView.convertDpToPixel(100.0f, DemoStickerView.this.getContext());
                    if (access$14002 > access$1400 && (abs < 25.0d || Math.abs(abs - 180.0d) < 25.0d)) {
                        double round = (double) Math.round(Math.max((double) Math.abs(motionEvent.getRawX() - DemoStickerView.this.scale_orgX), (double) Math.abs(motionEvent.getRawY() - DemoStickerView.this.scale_orgY)));
                        LayoutParams layoutParams = (LayoutParams) DemoStickerView.this.getLayoutParams();
                        double d4 = (double) layoutParams.width;
                        Double.isNaN(d4);
                        Double.isNaN(round);
                        layoutParams.width = (int) (d4 + round);
                        LayoutParams layoutParams2 = (LayoutParams) DemoStickerView.this.getLayoutParams();
                        double d5 = (double) layoutParams2.height;
                        Double.isNaN(d5);
                        Double.isNaN(round);
                        layoutParams2.height = (int) (d5 + round);
                        DemoStickerView.this.onScaling(true);
                    } else if (access$14002 < access$1400 && (abs < 25.0d || Math.abs(abs - 180.0d) < 25.0d)) {
                        int i2 = access$1500 / 2;
                        if (DemoStickerView.this.getLayoutParams().width > i2 && DemoStickerView.this.getLayoutParams().height > i2) {
                            double round2 = (double) Math.round(Math.max((double) Math.abs(motionEvent.getRawX() - DemoStickerView.this.scale_orgX), (double) Math.abs(motionEvent.getRawY() - DemoStickerView.this.scale_orgY)));
                            LayoutParams layoutParams3 = (LayoutParams) DemoStickerView.this.getLayoutParams();
                            double d6 = (double) layoutParams3.width;
                            Double.isNaN(d6);
                            Double.isNaN(round2);
                            layoutParams3.width = (int) (d6 - round2);
                            LayoutParams layoutParams4 = (LayoutParams) DemoStickerView.this.getLayoutParams();
                            double d7 = (double) layoutParams4.height;
                            Double.isNaN(d7);
                            Double.isNaN(round2);
                            layoutParams4.height = (int) (d7 - round2);
                            DemoStickerView.this.onScaling(false);
                        }
                    }
                    double rawY2 = (double) motionEvent.getRawY();
                    double access$11002 = DemoStickerView.this.centerY;
                    Double.isNaN(rawY2);
                    double d8 = rawY2 - access$11002;
                    double rawX2 = (double) motionEvent.getRawX();
                    double access$10002 = DemoStickerView.this.centerX;
                    Double.isNaN(rawX2);
                    double atan22 = (Math.atan2(d8, rawX2 - access$10002) * 180.0d) / 3.141592653589793d;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("log angle: ");
                    sb2.append(atan22);
                    String str3 = str2;
                    Log.v(str3, sb2.toString());
                    DemoStickerView.this.setRotation(((float) atan22) - 70.0f);
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("getRotation(): ");
                    sb3.append(DemoStickerView.this.getRotation());
                    Log.v(str3, sb3.toString());
                    DemoStickerView.this.onRotating();
                    DemoStickerView demoStickerView11 = DemoStickerView.this;
                    demoStickerView11.rotate_orgX = demoStickerView11.rotate_newX;
                    DemoStickerView demoStickerView12 = DemoStickerView.this;
                    demoStickerView12.rotate_orgY = demoStickerView12.rotate_newY;
                    DemoStickerView.this.scale_orgX = motionEvent.getRawX();
                    DemoStickerView.this.scale_orgY = motionEvent.getRawY();
                    DemoStickerView.this.postInvalidate();
                    DemoStickerView.this.requestLayout();
                }
            }
            return true;
        }
    };
    /* access modifiers changed from: private */
    public float move_orgX = -1.0f;
    /* access modifiers changed from: private */
    public float move_orgY = -1.0f;
    OnTouchSticker onTouchSticker;
    /* access modifiers changed from: private */
    public float rotate_newX = -1.0f;
    /* access modifiers changed from: private */
    public float rotate_newY = -1.0f;
    /* access modifiers changed from: private */
    public float rotate_orgX = -1.0f;
    /* access modifiers changed from: private */
    public float rotate_orgY = -1.0f;
    /* access modifiers changed from: private */
    public double scale_orgHeight = -1.0d;
    /* access modifiers changed from: private */
    public double scale_orgWidth = -1.0d;
    /* access modifiers changed from: private */
    public float scale_orgX = -1.0f;
    /* access modifiers changed from: private */
    public float scale_orgY = -1.0f;
    /* access modifiers changed from: private */
    public float this_orgX = -1.0f;
    /* access modifiers changed from: private */
    public float this_orgY = -1.0f;

    private class BorderView extends View {
        public BorderView(Context context) {
            super(context);
        }

        public BorderView(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public BorderView(Context context, AttributeSet attributeSet, int i) {
            super(context, attributeSet, i);
        }

        /* access modifiers changed from: protected */
        public void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            LayoutParams layoutParams = (LayoutParams) getLayoutParams();
            StringBuilder sb = new StringBuilder();
            sb.append("params.leftMargin: ");
            sb.append(layoutParams.leftMargin);
            Log.v(DemoStickerView.TAG, sb.toString());
            Rect rect = new Rect();
            rect.left = getLeft() - layoutParams.leftMargin;
            rect.top = getTop() - layoutParams.topMargin;
            rect.right = getRight() - layoutParams.rightMargin;
            rect.bottom = getBottom() - layoutParams.bottomMargin;
            Paint paint = new Paint();
            paint.setStrokeWidth(8.0f);
            paint.setColor(getResources().getColor(R.color.colorPrimary));
            paint.setStyle(Style.STROKE);
            canvas.drawRect(rect, paint);
        }
    }

    public interface OnTouchSticker {
        void onTouchedSticker();
    }

    /* access modifiers changed from: protected */
    public abstract View getMainView();

    /* access modifiers changed from: protected */
    public void onRotating() {
    }

    /* access modifiers changed from: protected */
    public void onScaling(boolean z) {
    }

    public DemoStickerView(Context context, OnTouchSticker onTouchSticker2) {
        super(context);
        init(context);
        this.onTouchSticker = onTouchSticker2;
    }

    public DemoStickerView(Context context, AttributeSet attributeSet, OnTouchSticker onTouchSticker2) {
        super(context, attributeSet);
        init(context);
        this.onTouchSticker = onTouchSticker2;
    }

    public DemoStickerView(Context context, AttributeSet attributeSet, int i, OnTouchSticker onTouchSticker2) {
        super(context, attributeSet, i);
        init(context);
        this.onTouchSticker = onTouchSticker2;
    }

    private void init(Context context) {
        this.iv_border = new BorderView(context);
        this.iv_scale = new ImageView(context);
        this.iv_delete = new ImageView(context);
        this.iv_flip = new ImageView(context);
        this.iv_scale.setImageResource(R.drawable.zoominout);
        this.iv_delete.setImageResource(R.drawable.remove);
        this.iv_flip.setImageResource(R.drawable.flip);
        setTag("DraggableViewGroup");
        this.iv_border.setTag("iv_border");
        this.iv_scale.setTag("iv_scale");
        this.iv_delete.setTag("iv_delete");
        this.iv_flip.setTag("iv_flip");
        int convertDpToPixel = convertDpToPixel(30.0f, getContext()) / 2;
        int convertDpToPixel2 = convertDpToPixel(100.0f, getContext());
        LayoutParams layoutParams = new LayoutParams(convertDpToPixel2, convertDpToPixel2);
        layoutParams.gravity = 17;
        LayoutParams layoutParams2 = new LayoutParams(-1, -1);
        layoutParams2.setMargins(40, 40, 40, 40);
        LayoutParams layoutParams3 = new LayoutParams(-2, -2);
        layoutParams3.setMargins(convertDpToPixel, convertDpToPixel, convertDpToPixel, convertDpToPixel);
        LayoutParams layoutParams4 = new LayoutParams(convertDpToPixel(30.0f, getContext()), convertDpToPixel(30.0f, getContext()));
        layoutParams4.gravity = 85;
        LayoutParams layoutParams5 = new LayoutParams(convertDpToPixel(30.0f, getContext()), convertDpToPixel(30.0f, getContext()));
        layoutParams5.gravity = 53;
        LayoutParams layoutParams6 = new LayoutParams(convertDpToPixel(30.0f, getContext()), convertDpToPixel(30.0f, getContext()));
        layoutParams6.gravity = 51;
        setLayoutParams(layoutParams);
        addView(getMainView(), layoutParams2);
        addView(this.iv_border, layoutParams3);
        addView(this.iv_scale, layoutParams4);
        addView(this.iv_delete, layoutParams5);
        addView(this.iv_flip, layoutParams6);
        setOnTouchListener(this.mTouchListener);
        setOnClickListener(this);
        this.iv_scale.setOnTouchListener(this.mTouchListener);
        this.iv_delete.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (DemoStickerView.this.getParent() != null) {
                    ((ViewGroup) DemoStickerView.this.getParent()).removeView(DemoStickerView.this);
                }
            }
        });
        this.iv_flip.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                Log.v(DemoStickerView.TAG, "flip the view");
                View mainView = DemoStickerView.this.getMainView();
                float f = -180.0f;
                if (mainView.getRotationY() == -180.0f) {
                    f = 0.0f;
                }
                mainView.setRotationY(f);
                mainView.invalidate();
                DemoStickerView.this.requestLayout();
            }
        });
    }

    public boolean isFlip() {
        return getMainView().getRotationY() == -180.0f;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    /* access modifiers changed from: private */
    public double getLength(double d, double d2, double d3, double d4) {
        return Math.sqrt(Math.pow(d4 - d2, 2.0d) + Math.pow(d3 - d, 2.0d));
    }

    private float[] getRelativePos(float f, float f2) {
        StringBuilder sb = new StringBuilder();
        sb.append("getRelativePos getX:");
        sb.append(((View) getParent()).getX());
        String str = "ken";
        Log.v(str, sb.toString());
        StringBuilder sb2 = new StringBuilder();
        sb2.append("getRelativePos getY:");
        sb2.append(((View) getParent()).getY());
        Log.v(str, sb2.toString());
        float[] fArr = {f - ((View) getParent()).getX(), f2 - ((View) getParent()).getY()};
        StringBuilder sb3 = new StringBuilder();
        sb3.append("getRelativePos absY:");
        sb3.append(f2);
        String sb4 = sb3.toString();
        String str2 = TAG;
        Log.v(str2, sb4);
        StringBuilder sb5 = new StringBuilder();
        sb5.append("getRelativePos relativeY:");
        sb5.append(fArr[1]);
        Log.v(str2, sb5.toString());
        return fArr;
    }

    @SuppressLint("WrongConstant")
    public void setControlItemsHidden(boolean z) {
        if (z) {
            this.iv_border.setVisibility(4);
            this.iv_scale.setVisibility(4);
            this.iv_delete.setVisibility(4);
            this.iv_flip.setVisibility(4);
            return;
        }
        this.iv_border.setVisibility(0);
        this.iv_scale.setVisibility(0);
        this.iv_delete.setVisibility(0);
        this.iv_flip.setVisibility(0);
    }

    /* access modifiers changed from: protected */
    public View getImageViewFlip() {
        return this.iv_flip;
    }

    public void onClick(View view) {
        setControlItemsHidden(false);
    }

    /* access modifiers changed from: private */
    public static int convertDpToPixel(float f, Context context) {
        return (int) (f * (((float) context.getResources().getDisplayMetrics().densityDpi) / 160.0f));
    }

    @SuppressLint("WrongConstant")
    public void setControlsVisibility(boolean z) {
        if (!z) {
            this.iv_border.setVisibility(8);
            this.iv_delete.setVisibility(8);
            this.iv_flip.setVisibility(8);
            this.iv_scale.setVisibility(8);
            return;
        }
        this.iv_border.setVisibility(0);
        this.iv_delete.setVisibility(0);
        this.iv_flip.setVisibility(0);
        this.iv_scale.setVisibility(0);
    }
}
