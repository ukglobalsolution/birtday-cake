package mybirthdayckae.birthdaywishes.birthdayphotomaker.crop;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.theartofdev.edmodo.cropper.CropImageView;

import androidx.appcompat.app.AppCompatActivity;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.CakeFareme.EditframecakeActivity;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.MainActivity;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.Nameoncake.EditCackeActivity;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.R;

public class CropImageActivity extends AppCompatActivity implements OnClickListener {
    public static Bitmap cropbitmap;
    Context context;
    CropImageView cropImageView;
    ImageView doneCropImage;
    ImageView rotateImage;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_crop_image);
        this.context = this;
        bind();
        this.cropImageView.setImageBitmap(MainActivity.bitmap);
    }

    private void bind() {
        this.cropImageView = (CropImageView) findViewById(R.id.cropImageView);
        this.rotateImage = (ImageView) findViewById(R.id.rotateImage);
        this.doneCropImage = (ImageView) findViewById(R.id.doneCropImage);
        this.rotateImage.setOnClickListener(this);
        this.doneCropImage.setOnClickListener(this);
        Animation loadAnimation = AnimationUtils.loadAnimation(this, R.anim.bounce);
        Animation loadAnimation2 = AnimationUtils.loadAnimation(this, R.anim.left);
        AnimationUtils.loadAnimation(this, R.anim.right);
        this.cropImageView.startAnimation(loadAnimation);
        this.rotateImage.startAnimation(loadAnimation2);
        this.doneCropImage.startAnimation(loadAnimation2);
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.doneCropImage) {
            cropbitmap = this.cropImageView.getCroppedImage();
            if (MainActivity.btnId==1){
                startActivity(new Intent(this.context, EditframecakeActivity.class));
            }else if(MainActivity.btnId==2){
                startActivity(new Intent(this.context, EditCackeActivity.class));
            }

        } else if (id == R.id.rotateImage) {
            CropImageView cropImageView2 = this.cropImageView;
            cropImageView2.setRotation(cropImageView2.getRotation() + 90.0f);
        }
    }


}
