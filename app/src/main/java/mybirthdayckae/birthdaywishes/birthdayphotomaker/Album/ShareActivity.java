package mybirthdayckae.birthdaywishes.birthdayphotomaker.Album;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.Glob;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.MainActivity;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.R;

import java.io.File;
@SuppressLint("WrongConstant")
public class ShareActivity extends AppCompatActivity implements OnClickListener {
    static int id;
    private String TAG = "Shareactiity";
    private TextView album_btn;
    private ImageView back;
    Bitmap finalbit;
    Uri finaluri;
    /* access modifiers changed from: private */
    public Handler handler = new Handler();
    private Bitmap imageBit;
    private boolean isPNG = false;
    private ImageView ivFacebook;
    private ImageView ivFinalImage;
    private ImageView ivHike;
    private ImageView ivInstagram;
    private ImageView ivShareMore;
    private ImageView ivWhatsApp;
    private String path;
    /* access modifiers changed from: private */
    public ProgressBar progressBar;
    /* access modifiers changed from: private */
    public int progressStatus = 0;
    /* access modifiers changed from: private */
    public FrameLayout saveimageframe;
    private TextView txtSave;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_share);

        bindView();
        Intent intent = getIntent();
        this.path = intent.getStringExtra("path");
        this.isPNG = intent.getBooleanExtra("showTbg", false);
        //this.ivFinalImage.setImageBitmap(EditframecakeActivity.mainframebitmap);
        this.ivFinalImage.setImageURI(Uri.parse(Glob._url));
        rateUs();
        this.album_btn.startAnimation(AnimationUtils.loadAnimation(this, R.anim.blink));
    }

    public boolean isOnline() {
        @SuppressLint("WrongConstant") NetworkInfo activeNetworkInfo = ((ConnectivityManager) getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    private void rateUs() {
        String str = "dialog_count";
        id = Glob.getPref(this, str);
        String str2 = this.TAG;
        StringBuilder sb = new StringBuilder();
        String str3 = "onCreate: pref";
        sb.append(str3);
        sb.append(id);
        Log.d(str2, sb.toString());
        if (id == 1 && !isFinishing()) {
            Log.d(this.TAG, str3);
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    if (!ShareActivity.this.isFinishing()) {
                        ShareActivity.this.showDialogforshow();
                    }
                }
            }, 3000);
        }
        if (Glob.getBoolPref(this, "isRated")) {
            id++;
            if (id == 6) {
                id = 1;
            }
            Glob.setPref(this, str, id);
        }
    }

    private void bindView() {
        this.progressBar = (ProgressBar) findViewById(R.id.progressBar);
        this.saveimageframe = (FrameLayout) findViewById(R.id.saveimageframe);
        setProgressBar();
        this.ivFinalImage = (ImageView) findViewById(R.id.ivFinalImage);
        this.ivFinalImage.setImageBitmap(this.finalbit);
        this.ivFinalImage.setOnClickListener(this);
        this.ivWhatsApp = (ImageView) findViewById(R.id.iv_whatsapp);
        this.ivWhatsApp.setOnClickListener(this);
        this.ivFacebook = (ImageView) findViewById(R.id.iv_facebook);
        this.ivFacebook.setOnClickListener(this);
        this.ivInstagram = (ImageView) findViewById(R.id.iv_instagram);
        this.ivInstagram.setOnClickListener(this);
        this.ivHike = (ImageView) findViewById(R.id.iv_Hike);
        this.ivHike.setOnClickListener(this);
        this.ivShareMore = (ImageView) findViewById(R.id.iv_Share_More);
        this.ivShareMore.setOnClickListener(this);
        this.album_btn = (TextView) findViewById(R.id.album_btn);
        this.album_btn.setOnClickListener(this);
    }

    public void onClick(View view) {
        String str = " Created By : ";
        String str2 = "mybirthdayckae.birthdaywishes.birthdayphotomaker.fileprovider";
        String str3 = "android.intent.extra.STREAM";
        String str4 = "android.intent.extra.TEXT";
        String str5 = "image/*";
        String str6 = "android.intent.action.SEND";
        switch (view.getId()) {
            case R.id.album_btn /*2131296301*/:
                startActivity(new Intent(this, My_Creation_Activity.class));
                finish();
                return;
            case R.id.iv_Hike /*2131296449*/:
                try {
                    Intent intent = new Intent(str6);
                    intent.setType(str5);
                    StringBuilder sb = new StringBuilder();
                    sb.append(Glob.app_name);
                    sb.append(str);
                    sb.append(Glob.app_link);
                    intent.putExtra(str4, sb.toString());
                    intent.putExtra(str3, FileProvider.getUriForFile(this, str2, new File(Glob._url)));
                    intent.setPackage("com.bsb.hike");
                    startActivity(intent);
                    return;
                } catch (Exception unused) {
                    Toast.makeText(this, "Hike doesn't installed", 1).show();
                    return;
                }
            case R.id.iv_Share_More /*2131296450*/:
                Intent intent2 = new Intent(str6);
                intent2.setType(str5);
                StringBuilder sb2 = new StringBuilder();
                sb2.append(Glob.app_name);
                sb2.append(" Create By : ");
                sb2.append(Glob.app_link);
                intent2.putExtra(str4, sb2.toString());
                intent2.putExtra(str3, FileProvider.getUriForFile(this, str2, new File(Glob._url)));
                startActivity(Intent.createChooser(intent2, "Share Image using"));
                return;
            case R.id.iv_facebook /*2131296451*/:
                try {
                    Intent intent3 = new Intent(str6);
                    intent3.setType(str5);
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(Glob.app_name);
                    sb3.append(str);
                    sb3.append(Glob.app_link);
                    intent3.putExtra(str4, sb3.toString());
                    intent3.putExtra(str3, FileProvider.getUriForFile(this, str2, new File(Glob._url)));
                    intent3.setPackage("com.facebook.katana");
                    startActivity(intent3);
                    return;
                } catch (Exception unused2) {
                    Toast.makeText(this, "Facebook doesn't installed", 1).show();
                    return;
                }
            case R.id.iv_instagram /*2131296453*/:
                try {
                    Intent intent4 = new Intent(str6);
                    intent4.setType(str5);
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append(Glob.app_name);
                    sb4.append(str);
                    sb4.append(Glob.app_link);
                    intent4.putExtra(str4, sb4.toString());
                    intent4.putExtra(str3, FileProvider.getUriForFile(this, str2, new File(Glob._url)));
                    intent4.setPackage("com.instagram.android");
                    startActivity(intent4);
                    return;
                } catch (Exception unused3) {
                    Toast.makeText(this, "Instagram doesn't installed", 1).show();
                    return;
                }
            case R.id.iv_whatsapp /*2131296457*/:
                try {
                    Intent intent5 = new Intent(str6);
                    intent5.setType(str5);
                    StringBuilder sb5 = new StringBuilder();
                    sb5.append(Glob.app_name);
                    sb5.append(str);
                    sb5.append(Glob.app_link);
                    intent5.putExtra(str4, sb5.toString());
                    intent5.putExtra(str3, FileProvider.getUriForFile(this, str2, new File(Glob._url)));
                    intent5.setPackage("com.whatsapp");
                    startActivity(intent5);
                    return;
                } catch (Exception unused4) {
                    Toast.makeText(this, "WhatsApp doesn't installed", 1).show();
                    return;
                }
            default:
                return;
        }
    }

    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    private void setProgressBar() {
        new Thread(new Runnable() {
            public void run() {
                while (ShareActivity.this.progressStatus < 100) {
                    ShareActivity shareActivity = ShareActivity.this;
                    shareActivity.progressStatus = shareActivity.progressStatus + 1;
                    try {
                        Thread.sleep(30);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    ShareActivity.this.handler.post(new Runnable() {
                        public void run() {
                            ShareActivity.this.progressBar.setProgress(ShareActivity.this.progressStatus);
                            if (ShareActivity.this.progressStatus == 100) {
                                ShareActivity.this.saveimageframe.setVisibility(8);
                            }
                        }
                    });
                }
            }
        }).start();
    }

    public void showDialogforshow() {
        @SuppressLint("ResourceType") final Dialog dialog = new Dialog(this, 16973839);
        dialog.requestWindowFeature(1);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        double d = (double) displayMetrics.heightPixels;
        Double.isNaN(d);
        int i = (int) (d * 1.0d);
        double d2 = (double) displayMetrics.widthPixels;
        Double.isNaN(d2);
        int i2 = (int) (d2 * 1.0d);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawableResource(R.color.primarytrans);
        dialog.getWindow().setLayout(i2, i);
        dialog.setContentView(R.layout.rate_dialog);
        TextView textView = (TextView) dialog.findViewById(R.id.rate);
        TextView textView2 = (TextView) dialog.findViewById(R.id.remindlater);
        TextView textView3 = (TextView) dialog.findViewById(R.id.nothanks);
        ((ImageView) dialog.findViewById(R.id.img)).setAnimation(AnimationUtils.loadAnimation(this, R.anim.blink));
        textView.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                ShareActivity.this.gotoStore();
                ShareActivity.id++;
                Glob.setPref(ShareActivity.this, "dialog_count", ShareActivity.id);
                Glob.setBoolPref(ShareActivity.this, "isRated", true);
                dialog.dismiss();
            }
        });
        textView2.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                Glob.dialog = true;
                Glob.setBoolPref(ShareActivity.this, "isRated", false);
                Glob.setPref(ShareActivity.this, "dialog_count", 1);
                dialog.cancel();
            }
        });
        textView3.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                Glob.setBoolPref(ShareActivity.this, "isRated", false);
                Glob.setPref(ShareActivity.this, "dialog_count", 1);
                dialog.cancel();
            }
        });
        if (Glob.dialog) {
            dialog.show();
        }
    }


    public void gotoStore() {
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(Glob.app_link));
        intent.setFlags(268468224);
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException unused) {
            Toast.makeText(this, "You don't have Google Play installed", 1).show();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

}
