package mybirthdayckae.birthdaywishes.birthdayphotomaker.Album;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.Glob;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.MainActivity;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.R;

import java.io.File;
import java.util.ArrayList;

public class My_Creation_Activity extends AppCompatActivity {
    public static ArrayList<String> IMAGEALLARY = new ArrayList<>();
    public static int pos;
    private ImageView fback;
    /* access modifiers changed from: private */
    public Galleryadapter galleryAdapter;
    private GridView lv;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_my__creation_);
        this.lv = (GridView) findViewById(R.id.gridview);
        this.fback = (ImageView) findViewById(R.id.fback);
        IMAGEALLARY.clear();
        StringBuilder sb = new StringBuilder();
        sb.append(Environment.getExternalStorageDirectory().getAbsolutePath());
        String str = "/";
        sb.append(str);
        sb.append(Glob.Edit_Folder_name);
        sb.append(str);
        listAllImages(new File(sb.toString()));
        this.galleryAdapter = new Galleryadapter(this, IMAGEALLARY);
        this.lv.setAdapter(this.galleryAdapter);
        this.fback.setOnClickListener(new OnClickListener() {
            @SuppressLint("WrongConstant")
            public void onClick(View view) {
                Intent intent = new Intent(My_Creation_Activity.this.getApplicationContext(), MainActivity.class);
                intent.setFlags(268468224);
                My_Creation_Activity.this.startActivity(intent);
                My_Creation_Activity.this.finish();
            }
        });
        this.lv.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                My_Creation_Activity.this.galleryAdapter.getItemId(i);
                My_Creation_Activity.pos = i;
                @SuppressLint("ResourceType") Dialog dialog = new Dialog(My_Creation_Activity.this, 16973839);
                DisplayMetrics displayMetrics = new DisplayMetrics();
                My_Creation_Activity.this.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                double d = (double) displayMetrics.heightPixels;
                Double.isNaN(d);
                int i2 = (int) (d * 1.0d);
                double d2 = (double) displayMetrics.widthPixels;
                Double.isNaN(d2);
                int i3 = (int) (d2 * 1.0d);
                dialog.requestWindowFeature(1);
                dialog.getWindow().setFlags(1024, 1024);
                dialog.setContentView(R.layout.activity_full_screen_view);
                dialog.getWindow().setLayout(i3, i2);
                dialog.setCanceledOnTouchOutside(true);
                ((ImageView) dialog.findViewById(R.id.iv_image)).setImageURI(Uri.parse((String) My_Creation_Activity.IMAGEALLARY.get(My_Creation_Activity.pos)));
                dialog.show();
            }
        });
    }

    private void listAllImages(File file) {
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            for (int length = listFiles.length - 1; length >= 0; length--) {
                String file2 = listFiles[length].toString();
                File file3 = new File(file2);
                StringBuilder sb = new StringBuilder();
                String str = "";
                sb.append(str);
                sb.append(file3.length());
                String sb2 = sb.toString();
                StringBuilder sb3 = new StringBuilder();
                sb3.append(str);
                sb3.append(file3.length());
                Log.d(sb2, sb3.toString());
                if (file3.length() <= 1024) {
                    Log.e("Invalid Image", "Delete Image");
                } else if (file3.toString().contains(".jpg") || file3.toString().contains(".png") || file3.toString().contains(".jpeg")) {
                    IMAGEALLARY.add(file2);
                }
                System.out.println(file2);
            }
            return;
        }
        System.out.println("Empty Folder");
    }

    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class));
        super.onBackPressed();
    }
}