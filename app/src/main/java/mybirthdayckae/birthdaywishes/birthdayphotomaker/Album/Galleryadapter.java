package mybirthdayckae.birthdaywishes.birthdayphotomaker.Album;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.WallpaperManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.media.MediaMetadataRetriever;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.OnScanCompletedListener;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import mybirthdayckae.birthdaywishes.birthdayphotomaker.Glob;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.R;

public class Galleryadapter extends BaseAdapter {
    private static LayoutInflater inflater;
    public static int pos;
    /* access modifiers changed from: private */
    public Activity activity;
    private int imageSize;
    ArrayList<String> imagegallary = new ArrayList<>();
    SparseBooleanArray mSparseBooleanArray;
    MediaMetadataRetriever metaRetriever;
    View vi;

    static class ViewHolder {
        ImageView imgDelete;
        ImageView imgIcon;
        ImageView imgShare;
        ImageView set_wallpaper;

        ViewHolder() {
        }
    }

    public long getItemId(int i) {
        return (long) i;
    }

    @SuppressLint("WrongConstant")
    public Galleryadapter(Activity activity2, ArrayList<String> arrayList) {
        this.activity = activity2;
        this.imagegallary = arrayList;
        inflater = (LayoutInflater) this.activity.getSystemService("layout_inflater");
        this.mSparseBooleanArray = new SparseBooleanArray(this.imagegallary.size());
    }

    public int getCount() {
        return this.imagegallary.size();
    }

    public Object getItem(int i) {
        return Integer.valueOf(i);
    }

    public View getView(final int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if (view == null) {
            view = LayoutInflater.from(this.activity).inflate(R.layout.list_gallary, viewGroup, false);
            viewHolder = new ViewHolder();
            viewHolder.imgIcon = (ImageView) view.findViewById(R.id.imgIcon);
            viewHolder.imgDelete = (ImageView) view.findViewById(R.id.imgDelete);
            viewHolder.imgShare = (ImageView) view.findViewById(R.id.imgShare);
            viewHolder.set_wallpaper = (ImageView) view.findViewById(R.id.set_wallpaper);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        viewHolder.imgShare.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {

                AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
                alertDialog.setTitle("Alert");
                alertDialog.setMessage("Are you sure you want to Share");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Galleryadapter galleryadapter = Galleryadapter.this;
                                StringBuilder sb = new StringBuilder();
                                sb.append("Love Photo Frame Created By : ");
                                sb.append(Glob.app_link);
                                galleryadapter.shareImage(sb.toString(), (String) Galleryadapter.this.imagegallary.get(i));
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();
                            }
                        });
                alertDialog.show();

            }
        });
        viewHolder.imgDelete.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {

                AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
                alertDialog.setTitle("Alert");
                alertDialog.setMessage("Are you sure you want to Delete ");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                File file = new File((String) Galleryadapter.this.imagegallary.get(i));
                                if (file.exists()) {
                                    file.delete();
                                }
                                Galleryadapter.this.imagegallary.remove(i);
                                Galleryadapter.this.notifyDataSetChanged();
                                if (Galleryadapter.this.imagegallary.size() == 0) {
                                    Toast.makeText(Galleryadapter.this.activity, "No Image Found..", Toast.LENGTH_LONG).show();
                                }
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();
                            }
                        });
                alertDialog.show();

            }
        });

        viewHolder.set_wallpaper.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
                alertDialog.setTitle("Alert");
                alertDialog.setMessage("Are you sure you want to set Wallpaper");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Galleryadapter.this.setWallpaper("", (String) Galleryadapter.this.imagegallary.get(i));
                                Galleryadapter.this.notifyDataSetChanged();
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();

            }
        });
        Glide.with(this.activity).load((String) this.imagegallary.get(i)).into(viewHolder.imgIcon);
        System.gc();
        return view;
    }

    /* access modifiers changed from: private */
    public void setWallpaper(String str, String str2) {
        WallpaperManager instance = WallpaperManager.getInstance(this.activity);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        this.activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int i = displayMetrics.heightPixels;
        int i2 = displayMetrics.widthPixels;
        try {
            Options options = new Options();
            options.inPreferredConfig = Config.ARGB_8888;
            instance.setBitmap(BitmapFactory.decodeFile(str2, options));
            instance.suggestDesiredDimensions(i2 / 2, i / 2);
            Toast.makeText(this.activity, "Wallpaper Set", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void shareImage(final String str, String str2) {
        MediaScannerConnection.scanFile(this.activity, new String[]{str2}, null, new OnScanCompletedListener() {
            @SuppressLint("WrongConstant")
            public void onScanCompleted(String str, Uri uri) {
                Intent intent = new Intent("android.intent.action.SEND");
                intent.setType("video/*");
                intent.putExtra("android.intent.extra.TEXT", str);
                intent.putExtra("android.intent.extra.STREAM", uri);
                intent.addFlags(524288);
                Galleryadapter.this.activity.startActivity(Intent.createChooser(intent, "Share Video"));
            }
        });
    }
}