package mybirthdayckae.birthdaywishes.birthdayphotomaker.Splash;

import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.R;

public class SplashScreen extends AppCompatActivity {

    boolean Ad_Show = false;

    /* access modifiers changed from: private */
    public void HomeScreen() {
        this.Ad_Show = false;
        startActivity(new Intent(this, Splash_activity.class));
        finish();
    }

    public void HomeScreenWithoutFinish() {
        this.Ad_Show = false;
        startActivity(new Intent(this, Splash_activity.class));
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);
        initView();
        isOnline();
    }

    private void initView() {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                    SplashScreen.this.HomeScreen();
            }
        }, 3000);

    }

    public boolean isOnline() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

}
