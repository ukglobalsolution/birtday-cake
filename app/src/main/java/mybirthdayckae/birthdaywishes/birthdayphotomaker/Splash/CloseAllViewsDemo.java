package mybirthdayckae.birthdaywishes.birthdayphotomaker.Splash;

import android.os.Bundle;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

public class CloseAllViewsDemo extends AppCompatActivity {
    /* access modifiers changed from: protected */
    @RequiresApi(api = 16)
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        finishAffinity();
        finish();
    }
}