package mybirthdayckae.birthdaywishes.birthdayphotomaker.Splash;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.Glob;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.MainActivity;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.R;

public class Splash_activity extends AppCompatActivity implements View.OnClickListener {
    private static final int MY_REQUEST_CODE1 = 3;
    private LinearLayout mLayoutBanner;
    private LinearLayout mAdContainerNative;
    private FrameLayout mLlAdContainer;
    private ImageView mLlstart;
    private ImageView mMoreApp;
    private ImageView mAppPrivacy;
    private ImageView mAppShare;
    public Dialog dialog;

    int counter = 0;
    boolean doubleBackToExitPressedOnce = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_activity);
        this.counter = 0;

        initView();
    }

    private void initView() {
        mLayoutBanner = (LinearLayout) findViewById(R.id.banner_layout);
        mAdContainerNative = (LinearLayout) findViewById(R.id.native_ad_container);
        mLlAdContainer = (FrameLayout) findViewById(R.id.llAdContainer);
        mLlstart = (ImageView) findViewById(R.id.llstart);
        mLlstart.setOnClickListener(this);
        mMoreApp = (ImageView) findViewById(R.id.moreApp);
        mMoreApp.setOnClickListener(this);
        mAppPrivacy = (ImageView) findViewById(R.id.appPrivacy);
        mAppPrivacy.setOnClickListener(this);
        mAppShare = (ImageView) findViewById(R.id.appShare);
        mAppShare.setOnClickListener(this);
    }

    public boolean isOnline() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("WrongConstant")
    @Override
    public void onClick(View v) {
        String str = "No Internet Connection..";
        switch (v.getId()) {
            case R.id.llstart:
                v.startAnimation(AnimationUtils.loadAnimation(this, R.anim.buttonclick));
                startActivity(new Intent(Splash_activity.this, MainActivity.class));
                break;
            case R.id.moreApp:
                v.startAnimation(AnimationUtils.loadAnimation(this, R.anim.buttonclick));

                if (!isOnline()) {
                    Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
                } else {
                    String Acccounturl = "https://play.google.com/store/apps/developer?id=Paly+Infotech&hl=en";
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(Acccounturl));
                    startActivity(i);
                }
                break;
            case R.id.appPrivacy:
                v.startAnimation(AnimationUtils.loadAnimation(this, R.anim.buttonclick));
                if (!isOnline()) {
                    Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
                } else {
                    String Privecyurl = "https://cakeapk.website/privacy-policy/";
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(Privecyurl));
                    startActivity(i);
                }
                break;
            case R.id.appShare:
                v.startAnimation(AnimationUtils.loadAnimation(this, R.anim.buttonclick));
                share();
                break;
            default:
                break;
        }


    }


    private void share() {
        Intent intent = new Intent("android.intent.action.SEND");
        intent.setType("text/plain");
        StringBuilder sb = new StringBuilder();
        sb.append(Glob.app_name);
        sb.append(" Created By :");
        sb.append(Glob.app_link);
        intent.putExtra("android.intent.extra.TEXT", sb.toString());
        startActivity(Intent.createChooser(intent, "Share Text"));
    }


    @Override
    public void onBackPressed() {
        String str = "click BACK again to exit";

        if (!isOnline()) {
            if (this.doubleBackToExitPressedOnce) {
                finish();
                super.onBackPressed();
            }
            this.doubleBackToExitPressedOnce = true;
            Snackbar make = Snackbar.make((View) this.mLayoutBanner, (CharSequence) str, Snackbar.LENGTH_SHORT);
            ((TextView) make.getView().findViewById(R.id.snackbar_text)).setTextColor(-1);
            make.show();
            Toast.makeText(this, str, Toast.LENGTH_LONG);
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    Splash_activity.this.doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        } else if (this.counter == 0) {
            opendialogexit();
        }

    }

    @SuppressLint("ResourceType")
    private void opendialogexit() {
        this.dialog = new Dialog(this, 16973840);
        this.dialog.requestWindowFeature(1);
        this.dialog.setContentView(R.layout.customdailog2);
        this.dialog.setCancelable(false);
        this.dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        ((Button) this.dialog.findViewById(R.id.btn_no)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        ((Button) this.dialog.findViewById(R.id.btn_yes)).setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @RequiresApi(api = 16)
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), CloseAllViewsDemo.class);
                intent.addFlags(268468224);
                startActivity(intent);
                finishAffinity();
                dialog.dismiss();
            }
        });
        this.dialog.show();

    }
}
