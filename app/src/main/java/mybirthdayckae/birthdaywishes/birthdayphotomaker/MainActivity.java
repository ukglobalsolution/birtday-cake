package mybirthdayckae.birthdaywishes.birthdayphotomaker;

import android.Manifest;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore.Images.Media;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.Album.My_Creation_Activity;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.Birtdaywish.BirthdayWishActivity;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.Nameoncake.SelectCakeViewActivity;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.Splash.Splash_activity;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.crop.CropImageActivity;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.R;

import java.io.IOException;

public class MainActivity extends AppCompatActivity implements OnClickListener {
    private static final int PICK_IMAGE_CAMERA = 100;
    private static final int PICK_IMAGE_GALLERY = 200;
    public static Bitmap bitmap;
    public static Uri imageUri;
     Dialog openDialog;
    ImageView btnMycollection, btnNameOnCacke, btnCackeWithImage, btnWish;
    Context context;
    public static int btnId;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_main);
        this.context = this;
        checkPermission();
        bind();
    }

    private void bind() {

        this.btnCackeWithImage = (ImageView) findViewById(R.id.btnCackeWithImage);
        this.btnNameOnCacke = (ImageView) findViewById(R.id.btnNameOnCacke);
        this.btnWish = (ImageView) findViewById(R.id.btnWish);
        this.btnMycollection = (ImageView) findViewById(R.id.btnMycollection);

        this.btnMycollection.setOnClickListener(this);
        this.btnNameOnCacke.setOnClickListener(this);
        this.btnCackeWithImage.setOnClickListener(this);
        this.btnWish.setOnClickListener(this);
    }

    private void checkPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA}, 1);
    }

    public void onRequestPermissionsResult(int i, @NonNull String[] strArr, @NonNull int[] iArr) {
        if (i != 1) {
            super.onRequestPermissionsResult(i, strArr, iArr);
            return;
        }
        if (iArr.length <= 0 || iArr[0] != 0) {
            Toast.makeText(this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, @Nullable Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i != 100) {
            if (i == 200 && i2 == -1) {
                try {
                    bitmap = Media.getBitmap(getContentResolver(), intent.getData());
                    startActivity(new Intent(this.context, CropImageActivity.class));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else if (i2 == -1) {
            try {
                bitmap = Media.getBitmap(getContentResolver(), imageUri);
                startActivity(new Intent(this.context, CropImageActivity.class));
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnCackeWithImage:
                view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.buttonclick));
                btnId = 1;
                chooseImage();
                break;
            case R.id.btnNameOnCacke:
                view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.buttonclick));
                btnId = 2;
                startActivity(new Intent(this.context, SelectCakeViewActivity.class));
                break;
            case R.id.btnWish:
                view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.buttonclick));
                startActivity(new Intent(this.context, BirthdayWishActivity.class));
                break;
            case R.id.btnMycollection :
                view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.buttonclick));
                startActivity(new Intent(this.context, My_Creation_Activity.class));
                return;
            default:
                return;
        }
    }

    private void chooseImage() {
     openDialog = new Dialog(context, R.style.Theme_AppCompat_Dialog_Alert);
        openDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        openDialog.setContentView(R.layout.choose_image_dilog);

        ImageView btnPicFromCamera = (ImageView) openDialog.findViewById(R.id.btnPicFromCamera);
        ImageView btnPicFromGallary = (ImageView) openDialog.findViewById(R.id.btnPicFromGallary);

        btnPicFromCamera.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("title", "New Picture");
                contentValues.put("description", "From your Camera");
                imageUri = getContentResolver().insert(Media.EXTERNAL_CONTENT_URI, contentValues);
                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                intent.putExtra("output", imageUri);
                startActivityForResult(intent, 100);
                openDialog.dismiss();
            }
        });
        btnPicFromGallary.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent("android.intent.action.PICK", Media.EXTERNAL_CONTENT_URI), 200);
                openDialog.dismiss();
            }
        });
        openDialog.show();
    }

    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(MainActivity.this, Splash_activity.class));
    }
}
