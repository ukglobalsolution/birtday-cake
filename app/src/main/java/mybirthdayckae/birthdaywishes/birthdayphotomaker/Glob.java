package mybirthdayckae.birthdaywishes.birthdayphotomaker;

import android.content.Context;
import android.content.SharedPreferences.Editor;
import android.net.Uri;

import mybirthdayckae.birthdaywishes.birthdayphotomaker.CakeFareme.EditframecakeActivity;

public class Glob {
    public static final String Edit_Folder_name = "/My Birthday Cacke/";
    public static String _url= EditframecakeActivity._uri2 ;
    public static String acc_link = "https://play.google.com/store/apps/developer?id=Paly+Infotech&hl=en";
    public static int appID = 1265;
    public static String app_link = "https://play.google.com/store/apps/details?id=mybirthdayckae.birthdaywishes.birthdayphotomaker&hl=en";
    public static String app_name = "My Birthday Cacke";
    public static boolean dialog = true;
    public static int intScreenHeight = 0;
    public static int intScreenWidth = 0;
    public static String privacy_link = "https://cakeapk.website/privacy-policy/";
    public static String k;
    public static Uri l;

    public static boolean getBoolPref(Context context, String str) {
        return context.getSharedPreferences(context.getPackageName(), 0).getBoolean(str, false);
    }

    public static int getPref(Context context, String str) {
        return context.getSharedPreferences(context.getPackageName(), 0).getInt(str, 1);
    }

    public static void setBoolPref(Context context, String str, boolean z) {
        Editor edit = context.getSharedPreferences(context.getPackageName(), 0).edit();
        edit.putBoolean(str, z);
        edit.apply();
    }

    public static void setPref(Context context, String str, int i) {
        Editor edit = context.getSharedPreferences(context.getPackageName(), 0).edit();
        edit.putInt(str, i);
        edit.apply();
    }
}
