package mybirthdayckae.birthdaywishes.birthdayphotomaker.Adapter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.Interface.ItemClickFrame;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.R;

import java.io.IOException;
import java.util.ArrayList;

public class MyFrameAdapter extends RecyclerView.Adapter<MyFrameAdapter.MyViewHolder> {
    private ArrayList<String> assetlist;
    private Context context;
    /* access modifiers changed from: private */
    public ItemClickFrame itemClickFrame;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView frameItem;

        public MyViewHolder(@NonNull View view) {
            super(view);
            this.frameItem = (ImageView) view.findViewById(R.id.frameItem);
        }
    }

    public MyFrameAdapter(Context context2, ArrayList<String> arrayList, ItemClickFrame itemClickFrame2) {
        this.context = context2;
        this.assetlist = arrayList;
        this.itemClickFrame = itemClickFrame2;
    }

    @NonNull
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new MyViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.frame_list, viewGroup, false));
    }

    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int i) {
        try {
            myViewHolder.frameItem.setImageBitmap(BitmapFactory.decodeStream(this.context.getAssets().open(String.valueOf(this.assetlist.get(i)))));
        } catch (IOException e) {
            e.printStackTrace();
        }
        myViewHolder.frameItem.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                MyFrameAdapter.this.itemClickFrame.setOnClickFrame(i);
            }
        });
    }

    public int getItemCount() {
        return this.assetlist.size();
    }
}
