package mybirthdayckae.birthdaywishes.birthdayphotomaker.Adapter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.Interface.ItemClickSticker;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.R;

import java.io.IOException;
import java.util.ArrayList;

public class MyStickerAdapter extends RecyclerView.Adapter<MyStickerAdapter.MyViewHolder> {
    private ArrayList<String> assetlist;
    private Context context;
    /* access modifiers changed from: private */
    public ItemClickSticker itemClickSticker;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView stickerItem;

        public MyViewHolder(@NonNull View view) {
            super(view);
            this.stickerItem = (ImageView) view.findViewById(R.id.stickerItem);
        }
    }

    public MyStickerAdapter(Context context2, ArrayList<String> arrayList, ItemClickSticker itemClickSticker2) {
        this.context = context2;
        this.assetlist = arrayList;
        this.itemClickSticker = itemClickSticker2;
    }

    @NonNull
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new MyViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.sticker_list, viewGroup, false));
    }

    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int i) {
        try {
            myViewHolder.stickerItem.setImageBitmap(BitmapFactory.decodeStream(this.context.getAssets().open(String.valueOf(this.assetlist.get(i)))));
        } catch (IOException e) {
            e.printStackTrace();
        }
        myViewHolder.stickerItem.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                MyStickerAdapter.this.itemClickSticker.setOnClickSticker(i);
            }
        });
    }

    public int getItemCount() {
        return this.assetlist.size();
    }
}
