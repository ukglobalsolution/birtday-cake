package mybirthdayckae.birthdaywishes.birthdayphotomaker.Text;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Shader.TileMode;
import android.graphics.Typeface;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.DemoStickerTExt.HorizontalListView;
import mybirthdayckae.birthdaywishes.birthdayphotomaker.R;

import com.flask.colorpicker.ColorPickerView.WHEEL_TYPE;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
import java.io.IOException;
import java.util.ArrayList;

public class TextActivity extends AppCompatActivity implements OnClickListener {
    public static Bitmap finalBitmapText;
    /* access modifiers changed from: private */
    public boolean blnIsFromTextColor = false;
    /* access modifiers changed from: private */
    public Dialog dialogForFontList;
    private Dialog dialogForText;
    private EditText etForEnterText;
    /* access modifiers changed from: private */
    public ArrayList<String> fontList;
    private FontListAdapter fontListAdapter;
    private GridView gvFontList;

    private HorizontalListView hlvTexture;
    private ImageView ivBack;
    private ImageView ivClearText;
    private ImageView ivDone;
    private ImageView ivDoneForEnterText;
    private ImageView ivFont;
    private ImageView ivInnerTexture;
    private ImageView ivOpacity;
    private ImageView ivShadow;
    private ImageView ivShadowColor;
    private ImageView ivSize;
    private ImageView ivTextColor;
    private LinearLayout llShadowContainer;
    /* access modifiers changed from: private */
    public int mSelectedColor;
    private SeekBar sbOpacity;
    private SeekBar sbSize;
    private SeekBar sbTextShadow;
    private TextureAdapter textureAdapter;
    /* access modifiers changed from: private */
    public ArrayList<String> textureList;
    /* access modifiers changed from: private */
    public TextView tvEnterText;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_text);
        getWindow().setFlags(1024, 1024);
        bindView();
        this.blnIsFromTextColor = false;
        openDialogForText();
        this.sbSize.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
                TextActivity.this.tvEnterText.setTextSize((float) i);
            }
        });
        this.sbOpacity.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
                TextActivity.this.tvEnterText.setAlpha(((float) i) / 255.0f);
            }
        });
        this.hlvTexture.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                if (i == 0) {
                    TextActivity.this.tvEnterText.getPaint().setShader(null);
                    TextActivity.this.tvEnterText.setText(TextActivity.this.tvEnterText.getText().toString());
                    return;
                }
                try {
                    TextActivity.this.innerTexture(BitmapFactory.decodeStream(TextActivity.this.getAssets().open((String) TextActivity.this.textureList.get(i))));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        this.sbTextShadow.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
                TextActivity.this.tvEnterText.setShadowLayer((float) i, -1.0f, 1.0f, TextActivity.this.mSelectedColor);
            }
        });
    }

    @SuppressLint("WrongConstant")
    public void onClick(View view) {
        String str = "Please Add Text First";
        String str2 = "";
        switch (view.getId()) {
            case R.id.ivBack /*2131296437*/:
                super.onBackPressed();
                finish();
                return;
            case R.id.ivClearText /*2131296438*/:
                openDialogForText();
                return;
            case R.id.ivDone /*2131296439*/:
                if (this.tvEnterText.getText().toString().equals(str2)) {
                    Toast.makeText(this, str, 0).show();
                    return;
                }
                finalBitmapText = getMainFrameBitmap();
                setResult(-1);
                finish();
                return;
            case R.id.ivDoneForEnterText /*2131296440*/:
                if (this.etForEnterText.getText().toString().equals(str2)) {
                    this.etForEnterText.setError(str);
                    return;
                }
                this.tvEnterText.setText(this.etForEnterText.getText().toString());
                this.dialogForText.dismiss();
                return;
            case R.id.ivFont /*2131296442*/:
                this.llShadowContainer.setVisibility(8);
                this.sbOpacity.setVisibility(8);
                this.sbSize.setVisibility(8);
                this.hlvTexture.setVisibility(8);
                if (this.tvEnterText.getText().toString().equals(str2)) {
                    Toast.makeText(this, str, 0).show();
                    return;
                } else {
                    openDialogForFontList();
                    return;
                }
            case R.id.ivInnerTexture /*2131296443*/:
                this.sbSize.setVisibility(8);
                this.sbOpacity.setVisibility(8);
                this.llShadowContainer.setVisibility(8);
                if (this.tvEnterText.getText().toString().equals(str2)) {
                    Toast.makeText(this, str, 0).show();
                    return;
                } else if (this.hlvTexture.getVisibility() == 0) {
                    this.hlvTexture.setVisibility(8);
                    return;
                } else {
                    listTexture("textture");
                    this.textureAdapter = new TextureAdapter(this.textureList, this);
                    this.hlvTexture.setAdapter((ListAdapter) this.textureAdapter);
                    this.hlvTexture.setVisibility(0);
                    return;
                }
            case R.id.ivOpacity /*2131296444*/:
                this.llShadowContainer.setVisibility(8);
                this.hlvTexture.setVisibility(8);
                this.sbSize.setVisibility(8);
                if (this.tvEnterText.getText().toString().equals(str2)) {
                    Toast.makeText(this, str, 0).show();
                    return;
                } else if (this.sbOpacity.getVisibility() == 0) {
                    this.sbOpacity.setVisibility(8);
                    return;
                } else {
                    this.sbOpacity.setVisibility(0);
                    return;
                }
            case R.id.ivShadow /*2131296445*/:
                this.sbSize.setVisibility(8);
                this.sbOpacity.setVisibility(8);
                this.hlvTexture.setVisibility(8);
                if (this.tvEnterText.getText().toString().equals(str2)) {
                    Toast.makeText(this, str, 0).show();
                    return;
                } else if (this.llShadowContainer.getVisibility() == 0) {
                    this.llShadowContainer.setVisibility(8);
                    return;
                } else {
                    this.llShadowContainer.setVisibility(0);
                    return;
                }
            case R.id.ivShadowColor /*2131296446*/:
                if (this.tvEnterText.getText().toString().equals(str2)) {
                    Toast.makeText(this, str, 0).show();
                    return;
                } else {
                    colorPickerDialog();
                    return;
                }
            case R.id.ivSize /*2131296447*/:
                this.llShadowContainer.setVisibility(8);
                this.sbOpacity.setVisibility(8);
                this.hlvTexture.setVisibility(8);
                if (this.tvEnterText.getText().toString().equals(str2)) {
                    Toast.makeText(this, str, 0).show();
                    return;
                } else if (this.sbSize.getVisibility() == 0) {
                    this.sbSize.setVisibility(8);
                    return;
                } else {
                    this.sbSize.setVisibility(0);
                    return;
                }
            case R.id.ivTextColor /*2131296448*/:
                this.sbOpacity.setVisibility(8);
                this.sbSize.setVisibility(8);
                this.hlvTexture.setVisibility(8);
                this.llShadowContainer.setVisibility(8);
                this.blnIsFromTextColor = true;
                if (this.tvEnterText.getText().toString().equals(str2)) {
                    Toast.makeText(this, str, 0).show();
                    return;
                } else {
                    colorPickerDialog();
                    return;
                }
            default:
                return;
        }
    }

    private Bitmap getMainFrameBitmap() {
        this.tvEnterText.setDrawingCacheEnabled(true);
        Bitmap createBitmap = Bitmap.createBitmap(this.tvEnterText.getDrawingCache());
        if (VERSION.SDK_INT >= 19) {
            createBitmap.setConfig(Config.ARGB_8888);
        }
        this.tvEnterText.setDrawingCacheEnabled(false);
        int height = createBitmap.getHeight();
        int width = createBitmap.getWidth();
        int i = height;
        int i2 = i;
        int i3 = width;
        int i4 = i3;
        int i5 = 0;
        while (i5 < width) {
            int i6 = i2;
            int i7 = i;
            int i8 = i4;
            int i9 = i3;
            for (int i10 = 0; i10 < height; i10++) {
                if (createBitmap.getPixel(i5, i10) != 0) {
                    int i11 = i5 + 0;
                    if (i11 < i9) {
                        i9 = i11;
                    }
                    int i12 = width - i5;
                    if (i12 < i8) {
                        i8 = i12;
                    }
                    int i13 = i10 + 0;
                    if (i13 < i7) {
                        i7 = i13;
                    }
                    int i14 = height - i10;
                    if (i14 < i6) {
                        i6 = i14;
                    }
                }
            }
            i5++;
            i3 = i9;
            i4 = i8;
            i = i7;
            i2 = i6;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("left:");
        sb.append(i3);
        sb.append(" right:");
        sb.append(i4);
        sb.append(" top:");
        sb.append(i);
        sb.append(" bottom:");
        sb.append(i2);
        Log.d("Trimed bitmap", sb.toString());
        return Bitmap.createBitmap(createBitmap, i3, i, (width - i3) - i4, (height - i) - i2);
    }

    private void colorPickerDialog() {
        String str = "cancel";
        ColorPickerDialogBuilder.with(this).setTitle("Choose color").initialColor(getResources().getColor(R.color.white)).wheelType(WHEEL_TYPE.FLOWER).density(12).setOnColorSelectedListener(new OnColorSelectedListener() {
            public void onColorSelected(int i) {
            }
        }).setPositiveButton((CharSequence) "ok", (ColorPickerClickListener) new ColorPickerClickListener() {
            public void onClick(DialogInterface dialogInterface, int i, Integer[] numArr) {
                if (!TextActivity.this.blnIsFromTextColor) {
                    TextActivity.this.mSelectedColor = i;
                } else {
                    TextActivity.this.tvEnterText.setTextColor(i);
                    TextActivity.this.blnIsFromTextColor = false;
                }
                dialogInterface.dismiss();
            }
        }).setNegativeButton((CharSequence) str, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        }).build().show();
    }

    private void openDialogForText() {
        this.dialogForText = new Dialog(this);
        this.dialogForText.requestWindowFeature(1);
        this.dialogForText.setContentView(R.layout.dialog_for_add_text);
        this.etForEnterText = (EditText) this.dialogForText.findViewById(R.id.etForEnterText);
        this.ivDoneForEnterText = (ImageView) this.dialogForText.findViewById(R.id.ivDoneForEnterText);
        this.ivDoneForEnterText.setOnClickListener(this);
        this.dialogForText.show();
    }

    private void openDialogForFontList() {
        this.dialogForFontList = new Dialog(this);
        this.dialogForFontList.requestWindowFeature(1);
        this.dialogForFontList.setContentView(R.layout.dialog_font);
        this.gvFontList = (GridView) this.dialogForFontList.findViewById(R.id.gvFontList);
        this.gvFontList.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                TextActivity.this.tvEnterText.setTypeface(Typeface.createFromAsset(TextActivity.this.getAssets(), (String) TextActivity.this.fontList.get(i)));
                TextActivity.this.dialogForFontList.dismiss();
            }
        });
        listFonts("fonts");
        this.fontListAdapter = new FontListAdapter(this.fontList, this);
        this.gvFontList.setAdapter(this.fontListAdapter);
        this.dialogForFontList.show();
    }

    private void bindView() {
        this.ivBack = (ImageView) findViewById(R.id.ivBack);
        this.ivBack.setOnClickListener(this);
        this.ivDone = (ImageView) findViewById(R.id.ivDone);
        this.ivDone.setOnClickListener(this);
        this.tvEnterText = (TextView) findViewById(R.id.tvEnterText);
        this.ivClearText = (ImageView) findViewById(R.id.ivClearText);
        this.ivClearText.setOnClickListener(this);
        this.ivSize = (ImageView) findViewById(R.id.ivSize);
        this.ivSize.setOnClickListener(this);
        this.ivFont = (ImageView) findViewById(R.id.ivFont);
        this.ivFont.setOnClickListener(this);
        this.ivOpacity = (ImageView) findViewById(R.id.ivOpacity);
        this.ivOpacity.setOnClickListener(this);
        this.sbSize = (SeekBar) findViewById(R.id.sbSize);
        this.sbOpacity = (SeekBar) findViewById(R.id.sbOpacity);
        this.ivInnerTexture = (ImageView) findViewById(R.id.ivInnerTexture);
        this.ivInnerTexture.setOnClickListener(this);
        this.hlvTexture = (HorizontalListView) findViewById(R.id.hlvTexture);
        this.ivShadow = (ImageView) findViewById(R.id.ivShadow);
        this.ivShadow.setOnClickListener(this);
        this.llShadowContainer = (LinearLayout) findViewById(R.id.llShadowContainer);
        this.ivShadowColor = (ImageView) findViewById(R.id.ivShadowColor);
        this.ivShadowColor.setOnClickListener(this);
        this.sbTextShadow = (SeekBar) findViewById(R.id.sbTextShadow);
        this.ivTextColor = (ImageView) findViewById(R.id.ivTextColor);
        this.ivTextColor.setOnClickListener(this);
    }

    private void listFonts(String str) {
        this.fontList = new ArrayList<>();
        this.fontList.clear();
        String[] strArr = new String[0];
        try {
            String[] list = getResources().getAssets().list(str);
            if (list != null) {
                for (String append : list) {
                    ArrayList<String> arrayList = this.fontList;
                    StringBuilder sb = new StringBuilder();
                    sb.append(str);
                    sb.append("/");
                    sb.append(append);
                    arrayList.add(sb.toString());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void listTexture(String str) {
        this.textureList = new ArrayList<>();
        this.textureList.clear();
        String[] strArr = new String[0];
        try {
            String[] list = getResources().getAssets().list(str);
            if (list != null) {
                for (String append : list) {
                    ArrayList<String> arrayList = this.textureList;
                    StringBuilder sb = new StringBuilder();
                    sb.append(str);
                    sb.append("/");
                    sb.append(append);
                    arrayList.add(sb.toString());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void innerTexture(Bitmap bitmap) {
        BitmapShader bitmapShader = new BitmapShader(bitmap, TileMode.REPEAT, TileMode.REPEAT);
        this.tvEnterText.setLayerType(1, null);
        this.tvEnterText.getPaint().setShader(bitmapShader);
    }
}
