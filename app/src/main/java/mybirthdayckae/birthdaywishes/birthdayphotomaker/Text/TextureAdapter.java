package mybirthdayckae.birthdaywishes.birthdayphotomaker.Text;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import java.io.IOException;
import java.util.ArrayList;

import mybirthdayckae.birthdaywishes.birthdayphotomaker.R;

public class TextureAdapter extends BaseAdapter {
    Context mContext;
    ArrayList<String> textureList;

    public class ViewHolder {
        ImageView ivTextureItem;

        public ViewHolder() {
        }
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public TextureAdapter(ArrayList<String> arrayList, Context context) {
        this.textureList = arrayList;
        this.mContext = context;
    }

    public int getCount() {
        return this.textureList.size();
    }

    public Object getItem(int i) {
        return this.textureList.get(i);
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder = new ViewHolder();
        if (view == null) {
            view = LayoutInflater.from(this.mContext).inflate(R.layout.texture_item, null);
        }
        viewHolder.ivTextureItem = (ImageView) view.findViewById(R.id.cvTextureItem);
        try {
            viewHolder.ivTextureItem.setImageBitmap(BitmapFactory.decodeStream(this.mContext.getAssets().open((String) this.textureList.get(i))));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return view;
    }
}
