package mybirthdayckae.birthdaywishes.birthdayphotomaker.Text;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;

import mybirthdayckae.birthdaywishes.birthdayphotomaker.R;

public class FontListAdapter extends BaseAdapter {
    ArrayList<String> fontList;
    Context mContext;

    private class ViewHolder {
        TextView tvFontListItem;

        private ViewHolder() {
        }
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public FontListAdapter(ArrayList<String> arrayList, Context context) {
        this.fontList = arrayList;
        this.mContext = context;
    }

    public int getCount() {
        return this.fontList.size();
    }

    public Object getItem(int i) {
        return this.fontList.get(i);
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder = new ViewHolder();
        if (view == null) {
            view = LayoutInflater.from(this.mContext).inflate(R.layout.font_item, null);
        }
        viewHolder.tvFontListItem = (TextView) view.findViewById(R.id.tvFontListItem);
        viewHolder.tvFontListItem.setTypeface(Typeface.createFromAsset(this.mContext.getAssets(), (String) this.fontList.get(i)));
        return view;
    }
}
